#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: yasheng
"""

import numpy as np
import pandas as pd

#paths for the input and output files
actual_file = './input/actual.txt'
predicted_file = './input/predicted.txt'
window_file = './input/window.txt'
comparison_file = './output/comparison.txt'



#load the input files as DataFrame
actual = pd.read_csv(actual_file,
                     sep='|' , 
                     names = ['time','stock','actual_price']
                     )

predicted = pd.read_csv(predicted_file,sep='|' , 
                        names = ['time','stock','predicted_price'])

window = int(open(window_file,'r').read())


#preprossing the data
data = pd.merge(actual,predicted, on = ['time','stock'])
error = pd.Series.to_frame(abs(data['actual_price']-data['predicted_price']))
error.columns=['error']
data = pd.concat([data[['time','stock']], error], axis=1)

#calculating average_error
averge_error = []
start = []
last = []
i = min(data['time']) - 1

while i <= max(data['time']) - window:
    slice = data.loc[(data['time']-i-1)//window == min(data['time'])//window]
    if len(slice):
        averge_error.append('{0:.2f}'.format(np.mean(slice['error'])))
    else:
        averge_error.append('NA')
    start.append(i+1)
    last.append(i+window)
    i += 1

#writing output
output=pd.DataFrame(list(zip(start,last,averge_error)))
output.to_csv(comparison_file, sep='|', index=False, header=False)
print('Comparison finished')
